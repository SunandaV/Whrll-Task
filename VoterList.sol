pragma solidity ^0.8.4;

import "./User.sol";

contract VoterList is User {
    
    address[] public regVoters;
    mapping(address => address) citizenToCitizenCont;
    
        //this function returns the voters address in an array
        function getAllVoters() public view onlyUNMemAccess(msg.sender) returns (address[] memory){
            address [] memory voters;
            for(uint i =0;i< regVoters.length;i++){
                voters[i] = regVoters[i];
            }
            return voters;
        }
        
        //this function pushes the msg.sender to the regVotes array
        function registerVoter()public{
            regVoters.push(msg.sender);
        }
        
        //this function returns the array of voterContracts
        function getContractAdd() public view onlyUNMemAccess(msg.sender) returns (address[] memory){
            address [] memory votersContr;
            for(uint i =0;i< regVoters.length;i++){
                votersContr[i] = citizenToCitizenCont[regVoters[i]] ;
            }
            return votersContr;
        }
        
        //this function returns the details of the contratUsers 
        function getuserdetails() public view onlyUNMemAccess(msg.sender)  returns ( string memory , string memory , uint8  , bytes32  , address){
            User contractUser = User(citizenToCitizenCont[msg.sender]);
            return contractUser.getDetails();
        }
        
        //this function check if the msg.sender is registered or not
        function isRegistered() public view onlyUNMemAccess(msg.sender) returns(bool){
                for(uint i =0;i< regVoters.length;i++){
                    if(regVoters[i] == msg.sender){
                    return true;   
                    }else{
                    return false;
                }
            }
        }
    }

