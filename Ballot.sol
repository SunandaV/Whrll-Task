// SPDX-License-Identifier: GPL-3.0-or-later
pragma solidity ^0.8.4;

import "./VoterList.sol";
import "./User.sol";

contract Ballot{
    VoterList public immutable voterList;
    User public immutable user;
    uint256 public votingTime;
    address[] public candidateList;
    address[] public UNmembers;
    address public winner;
    bool vote;
    address[] public voteUser;
    
    struct candidateDetail{
        address candidate;
        uint voteCount;
        voters voters;
    }
    
    struct voters {
        address voter;
        bool vote;
        address votedTo;
    }
    
    mapping (address => candidateDetail) public candidateDetails;
    mapping (address => voters) public votes;
    
    constructor (address[] memory candidates_, address[] memory UNmembers_){
        voterList = new VoterList();
        user = new User();
        candidateList = candidates_;
        UNmembers = UNmembers_;
        votingTime = block.timestamp + 10*60; // 10 min
    }
    
    //this function will take candidate as input and update the vote in voters
    function Vote(address candidate_)public returns (bool){
        votes[msg.sender].voter = msg.sender;
        candidateDetails[candidate_].candidate = candidate_;
        candidateDetails[candidate_].voteCount = candidateDetails[candidate_].voteCount +1;
            require (votes[msg.sender].vote == false, "voter already voted");
                for (uint i=0; i < candidateList.length; i++){
                    votes[msg.sender].vote = true;
                    votes[msg.sender].votedTo = candidate_;
                    voteUser.push(msg.sender);
                    user.setVoteGiven(candidate_);
            }
            candidateDetails[candidate_].voters = votes[msg.sender];
            return votes[msg.sender].vote;
    }
    
    //this function get the vote of the user
    function getMyVote()public view returns (address){
       require(votes[msg.sender].vote == true, "you have not voted yet");
       return votes[msg.sender].votedTo;
    }
    
    //this function will get the result of the E voting
    function getResult()public view returns (address){
        require (votingTime<=block.timestamp,"voting is still going on");
        uint winningVoteCount = 0;
        for (uint x = 0; x < candidateList.length; x++) {
            if(candidateDetails[candidateList[x]].voteCount > winningVoteCount){
                 winningVoteCount = candidateDetails[candidateList[x]].voteCount;
                 winner == candidateList[x];
            }
            
        }
        return winner;
    }
    
    //this function returns the vote status of the user and the address he voted for
    function getUserVote()public view returns (address,bool){
            if (votes[msg.sender].vote == true){
                return (votes[msg.sender].votedTo,true);
            }
            else{
                return (address(0),false);
            }
    }
    
    //this function returns all the voters and candidate he voted for
    function getVoteMap()public view returns (address,address){
        for(uint i; i<voteUser.length;i++){
            if(votes[voteUser[i]].vote == true){
                return (votes[voteUser[i]].voter, votes[voteUser[i]].votedTo);
            }
        }
    }

    
}
