pragma solidity ^0.8.4;


contract User {
    
    address user;
    
    string name;
    string addressRes;
    uint8 age;
    bytes2 gender;
    address   voteGiven;
    address[] givenAccess;
    address [] public UNmembers;
    mapping(address => bool) public UNMemAccess; 
    mapping(address => bool) public accessToExpire;

    constructor() public{
        user = msg.sender;
    }
    
    modifier onlyUNMemAccess (address _addr) {
        require(UNMemAccess[_addr]== true);
        _;
    }
    
        //this function get the user details if msg.sender has the access
        function getDetails() public view returns ( string memory , string memory , uint8  , bytes32  , address ){
            require(msg.sender == user || accessToExpire[msg.sender] == true || UNMemAccess[msg.sender] ==  true );
            return (name,addressRes,age,gender,voteGiven);
        }
        
        //this function returns the _expiry of the address 
        function giveAccess(address _addr, bool _expiry)public returns(bool){
            accessToExpire[_addr] = _expiry;
        } 
        
        //this function assign the voteGiven variable to the _candidate address
        function setVoteGiven(address _candidate) public{
            voteGiven =_candidate;
        }
        
        //this function returns the address of the _candidate the msg.sender
        function getVoteGiven() view public returns(address){
            require(msg.sender == user || accessToExpire[msg.sender] == true || UNMemAccess[msg.sender] ==  true );
            return voteGiven;
        }
    
}
